# README #

Write an API application including test that connects 
to this application on localhost  port 3333

read result in xml format and return json with replaced parameter name as following:

:param_1 => some_string
:param_2 => some_date
:some_code => iso_code
:some_price_param => price

Commit your code to some repository and provide me an access.
Estimated time: about 30-40 minutes


API  : http://localhost:3000/api/v1/convert_data_to_json
METHOD : POST
BODY DATA : <examples type="array">
<example>
<id type="integer">1</id>
<name>a</name>
<param-1>a</param-1>
<param-2 type="dateTime">2013-05-20T00:00:00Z</param-2>
<some-code>b</some-code>
<some-price-param type="float">1.1</some-price-param>
</example>
<example>
<id type="integer">2</id>
<name>name</name>
<param-1>param_1</param-1>
<param-2 type="dateTime">2014-05-20T00:00:00Z</param-2>
<some-code>ABC</some-code>
<some-price-param type="float">1.1</some-price-param>
</example>
</examples>