class ExamplesController < ApplicationController
  before_action :set_example, only: [:show, :update, :destroy]

  # GET /examples
  # GET /examples.json
  def index
    @examples = Example.all
    @message = @examples.to_xml
    @json_data =  convert_hash_to_json(@message)
    @json_data =  change_in_key_mapping(@json_data)
    render :json =>  @json_data
  end

  # GET /examples/1
  # GET /examples/1.json
  def show
    render json: @example
  end

  # POST /examples
  # POST /examples.json
  def create
    @example = Example.new(example_params)

    if @example.save
      render json: @example, status: :created, location: @example
    else
      render json: @example.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /examples/1
  # PATCH/PUT /examples/1.json
  def update
    @example = Example.find(params[:id])

    if @example.update(example_params)
      head :no_content
    else
      render json: @example.errors, status: :unprocessable_entity
    end
  end

  # DELETE /examples/1
  # DELETE /examples/1.json
  def destroy
    @example.destroy

    head :no_content
  end

  private

    def set_example
      @example = Example.find(params[:id])
    end

    def example_params
      params[:example]
    end
  def convert_hash_to_json  (data)
    @data =  Hash.from_xml(data)
    @message =[]
    @data["examples"].each do |attributes|
      @message.push(attributes)
    end
  end
  def change_in_key_mapping(data)
    new_mappings = {"id"=>"id","name" => "name","param_1" => "some_string", "param_2" => "some_date", "some_code" => "iso_code", "some_price_param" => "price"}
    @new_json=[]
    data.each do |i|
      @new_json.push(Hash[i.map {|k, v| [new_mappings[k], v] }])
    end
    return @new_json
  end
end
