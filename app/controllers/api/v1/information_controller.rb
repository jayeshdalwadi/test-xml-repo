class Api::V1::InformationController <Api::V1::ApiController 
    def convert_data_to_json
      begin
        @data =  Hash.from_xml(request.body.read)
        @message =[]
        @data["examples"].each do |attributes|
          @message.push(attributes)
        end
        render :status=>200, :json=>@message
      rescue Exception => e
        render :status=>404, :json=>e.message
      end
    end
end
