require 'spec_helper'

describe ExamplesController do
  before do

    Example.create(name: 'a', param_1: 'a', param_2:'2013-05-20T00:00:00.000Z',some_code:'b',some_price_param:'1.1')
    Example.create(name: 'name', param_1: 'param_1', param_2:'2014-05-20T00:00:00.000Z',some_code:'ABC',some_price_param:'1.1')
  end

  describe "get to index" do
    it "should return json" do # depend on what you return in action
      get :index
      body = JSON.parse(response.body)
      body.each do |i|
        expect(i['some_string']).to_not be_nil
        expect(i['some_date']).to_not be_nil
        expect(i['iso_code']).to_not be_nil
        expect(i['price']).to_not be_nil

      end
    end
  end
end